package test;

import org.junit.Test;
import prog.Categorization;
import prog.Movie;
import prog.MovieStatistics;

import java.io.File;
import java.util.Map;

import static org.junit.Assert.*;

/*
1- theEqualsIsCorrect: create objects
m1: The Lord of the Rings,134,YOUNG,
m2:The Lord of the Rings,135,YOUNG, and
m3:The Lord of the Rings2,134,YOUNG inside the method.

* Test the equal method of m1:  the test checks equality of m1 with m2, with m3,
*  with null and with a  new  instance of Object class .
*
2- youngMovieIsNotFor15YearsOld: create an object of YOUNG category and test it is not for 15 years
*  old using isRecommendable method.

3- onlyThoseKeysArePresentInMapWhichAreInFile: use anyOnly.txt
* file to test the output of MovieStatistics.readStatistics method.
* You should test has only ANY category and the accumulated value of waters.
* Test the other categories(ADULT,YOUNG) are not present in there retuned map.
*/
public class Tests {
    @Test
    public void theEqualsIsCorrect() {
        Movie m1 = new Movie("The Lord of the Rings", 134, Categorization.YOUNG);
        Movie m2 = new Movie("The Lord of the Rings", 135, Categorization.YOUNG);
        Movie m3 = new Movie("The Lord of the Rings2", 134, Categorization.YOUNG);

        assertEquals(m1, m2);
        assertNotEquals(m1, m3);
        assertNotEquals(m2, m3);

        assertNotEquals(m1, null);
        assertNotEquals(m1, new Object());

    }

    @Test
    public void youngMovieIsNotFor15YearsOld()  {
        Movie m1 = new Movie("The Lord of the Rings", 134, Categorization.YOUNG);
        assertFalse(m1.isRecommendableFor(15));
        assertTrue(m1.isRecommendableFor(18));
    }

    /*
     * 3- onlyThoseKeysArePresentInMapWhichAreInFile: use anyOnly.txt
     * file to test the output of MovieStatistics.readStatistics method.
     * You should test has only ANY category and the accumulated value of waters.
     * Test the other categories(ADULT,YOUNG) are not present in there retuned map.*/
    @Test
    public void onlyThoseKeysArePresentInMapWhichAreInFile() {
        File in = new File("src/anyOnly.txt");
        Map<Categorization, Integer> stats = MovieStatistics.readStatistics(in);
//        System.out.println(stats);
        assertEquals(66, (int) stats.get(Categorization.ANY));
        assertFalse(stats.containsKey(Categorization.ADULT));
        assertFalse(stats.containsKey(Categorization.YOUNG));

    }
}