package prog;

import static prog.Categorization.ADULT;
import static prog.Categorization.YOUNG;

/*
Create a class with the name prog.Movie which represents a movie. It has the following members:
    title, a string
    length, positive integer, given in minutes
    category, its type is Categorization

Every data field should have a corresponding getter/setter method pair.
The class has a parametrized constructor that gets all values for its fields (in the defined order above)
 and sets them to the inner fields.
The constructor should throw IllegalArgumentException if the title is empty,
 or the length is less than 40 minutes.
Make a default constructor that if it is called three times ,

 it creates those objects Title1,60,ADULT,Title2,60,ADULT, and Title3,60,ADULT.

Create standard methods for the following purposes.
    The String representation of a prog.Movie instance should be:
    <title> (<length>, <category>).

    The length of the movie should be written like this:
    <length in integer hours>:<remaining minutes>.
     For example: Rambo (1:30, ADULT) or The Lord of the Rings (2:17, YOUNG)
    Define the equality check in a way to consider two movies equal if they have the same title.

*/
/*
* The Movie class implements the interface
 , returning false when its category field is YOUNG and the provided number
  is less than 16, or if the field is ADULT and the number is less than 18.*/
public class Movie implements Recommendable {
    static int callcnt = 0;
    String title;
    int length;
    Categorization category;

    public Movie() {
        this("Title" + ++callcnt, 60, ADULT);

    }

    public Movie(String title, int length, Categorization category) {
        if (title.isEmpty() || length < 40) {
            throw new java.lang.IllegalArgumentException();
        } else {
            this.title = title;
            this.length = length;
            this.category = category;
        }

    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }

    public Categorization getCategory() {
        return category;
    }

    public void setCategory(Categorization category) {
        this.category = category;
    }

    public String getHHMM() {
        return length / 60 + ":" + length % 60;
    }

    @Override
    public String toString() {
        return title + ' ' + "(" + getHHMM() + ", " + category + ')';
    }//<title> (<length>, <category>).

    public boolean equals(Object o) {

        if (o == null || !(o instanceof Movie)) { //instance of actually also checks the == null condition but I guess I lost point on this last time
            return false;
        }
        if (o == this) {
            return true;
        }
        Movie e = (Movie) o;
        return e.title.equals(title);
    }

    /*
    * The Movie class implements the interface
     , returning false when its category field is YOUNG and the provided number
      is less than 16, or if the field is ADULT and the number is less than 18.*/
    @Override
    public boolean isRecommendableFor(int age) {
        return !(this.category == YOUNG && age < 16) && !(this.category == ADULT && age < 18);
    }

    public int compareTo(Movie m) {
        return Integer.compare(this.length, m.length);

    }
}
