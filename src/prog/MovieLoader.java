package prog;

import java.util.ArrayList;
import java.util.List;

/*Create a prog.MovieLoader class with a static readMovies method.

    The method takes a list of strings as its only parameter.
        The format of the strings as follows: <title>,<length>,<category>
        For example: Rambo,90,ADULT or The Lord of the Rings,137,YOUNG.
    The method converts each string into a proper Movie instance,
     and returns them as a sorted list based on the length (Figure out what you need to do).

Make it impossible to instantiate the class.*/ //abstract or private consttructor? ((
public class MovieLoader {
    private MovieLoader() {
    }

    static ArrayList<Movie> readMovies(List<String> _movies) {
        ArrayList<Movie> movies = new ArrayList<>();
        for (int i = 0; i < _movies.size(); i++) {
            String mvstr = _movies.get(i);
            String delims = "[,]";
            String[] strarray = mvstr.split(delims);

            Movie mv = new
                    Movie(strarray[0],
                    Integer.parseInt(strarray[1]),
                    Categorization.valueOf(strarray[2]));
            movies.add(mv);
        }
        movies.sort(Movie::compareTo);
        return movies;
    }

}
