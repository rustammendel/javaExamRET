package prog;

/*Create a RestrictedMovie class, which extends the Movie class, and its constructor
throws WrongMovieRestrictionException exception if you try to create an instance with ANY as category.
 The exception's constructor gets the following message: Cannot create RestrictedMovie without restriction.
 Override the setCategory method to throw WrongMovieRestrictionException exception if it gets ANY as
 a parameter. The message has to be RestrictedMovie can not be changed to have no restriction.

*/
public class RestrictedMovie extends Movie {
    public RestrictedMovie(String title, int length, Categorization category) {
        super(title, length, verifyCat(category));
    }

    private static Categorization verifyCat(Categorization cat) {
        if (cat == Categorization.ANY) {
            throw new WrongMovieRestrictionException("Cannot create RestrictedMovie without restriction");
        } else
            return cat;
    }

    @Override
    public void setCategory(Categorization category) {
        if (category == Categorization.ANY) {
            throw new WrongMovieRestrictionException("RestrictedMovie can not be changed to have no restriction");

        } else {
            setCategory(category);
        }
    }
}
