package prog;
/*Create a prog.WrongMovieRestrictionException exception, which is an
unchecked exception (it is derived from the class RuntimeException).
Its constructor takes a string and passes it to the invocation of the constructor of its base class.
 */

public class WrongMovieRestrictionException extends RuntimeException {
    public WrongMovieRestrictionException(String msg) {
        super(msg);
    }
}
