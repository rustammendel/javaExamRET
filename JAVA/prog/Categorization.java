package prog;

/*Create an enum with the name prog.Categorization. Its values are:

    ANY
    YOUNG
    ADULT
*/
public enum Categorization {
    ANY, YOUNG, ADULT;
}
