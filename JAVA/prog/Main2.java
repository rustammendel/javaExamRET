package prog;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class Main2 {
    public static void main(String[] args) {
        List<String> list = new ArrayList<>();
        File in = new File("src/in.txt");
        Map<Categorization, Integer> stats = MovieStatistics.readStatistics(in);
        System.out.println(stats);
        Movie m1 = new Movie("T1", 120, Categorization.ADULT);
        System.out.println(m1);
        Movie m2 = new Movie();
        Movie m3 = new Movie();
        RestrictedMovie rm = new RestrictedMovie("T2", 75, Categorization.ADULT);
        System.out.println(m2);
        System.out.println(m3);
        System.out.println(rm);

        list.add("Rambo,90,ADULT");
        list.add("The Lord of the Rings,137,YOUNG");
        list.add("Title2,60,ANY");
        System.out.println(MovieLoader.readMovies(list));

    }
}/*
I get this output :
{YOUNG=5, ADULT=2}
T1 (2:0, ADULT)
Title1 (1:40, ADULT)
Title2 (1:40, ADULT)
T2 (1:15, ADULT)
[Title2 (1:0, ANY), Rambo (1:30, ADULT), The Lord of the Rings (2:17, YOUNG)]*/