package prog;

/*Create the interface prog.Recommendable which has only one method:
 isRecommendableFor(int). Its only parameter is an integer,

 and it returns a boolean.
 */
public interface Recommendable {
    boolean isRecommendableFor(int age);
}
