package prog;

/*
Task 2
Create a RestrictedMovie class, which extends the Movie class, and its constructor
throws WrongMovieRestrictionException exception if you try to create an instance with ANY as category.
 The exception's constructor gets the following message: Cannot create RestrictedMovie without restriction.
Override the setCategory method to throw WrongMovieRestrictionException exception if it gets ANY as a parameter. The message has to be RestrictedMovie can not be changed to have no restriction.
Task 3
Create a prog.MovieLoader class with a static readMovies method.
    The method takes a list of strings as its only parameter.
        The format of the strings as follows: <title>,<length>,<category>
        For example: Rambo,90,ADULT or The Lord of the Rings,137,YOUNG.
    The method converts each string into a proper Movie instance, and returns them as a sorted list based on the length (Figure out what you need to do).
Make it impossible to instantiate the class.
Task 4

Create a class, called pro.MovieStatistics,
 which has a static method: readStatistics and receives a java.io.File object as parameter.
 It should return how many watchers have watched the different Categorizations.
  The method can throw IOException. The format of the file is as follows:
    every line contains one data
    one line looks like this: <title>,<length>,<Categorization>,<watcher-count>
The type of the returned object should be java.util.Map<Catgorization, Integer>.
If the file does not exists or empty, than it should return an empty map.
Categories that are not present in the file should not be present in the result Map either.

Task 5

create a test.Tests class with the following test cases .

1- theEqualsIsCorrect: create objects m1: The Lord of the Rings,134,YOUNG, m2:The Lord of the Rings,135,YOUNG, and m3:The Lord of the Rings2,134,YOUNG inside the method. Test the equal method of m1:  the test checks equality of m1 with m2, with m3, with null and with a  new  instance of Object class .

2- youngMovieIsNotFor15YearsOld: create an object of YOUNG category and test it is not for 15 years old using isRecommendable method.

3- onlyThoseKeysArePresentInMapWhichAreInFile: use anyOnly.txt file to test the output of MovieStatistics.readStatistics method.You should test has only ANY category and the accumulated value of waters. Test the other categories(ADULT,YOUNG) are not present in there retuned map.
*/public class Main {
    public static void main(String[] args) {

    }
}
