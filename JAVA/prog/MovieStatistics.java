package prog;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

/*Create a class, called pro.MovieStatistics,
 which has a static method: readStatistics and receives a java.io.File object as parameter.
 It should return how many watchers have watched the different Categorizations.
  The method can throw IOException. The format of the file is as follows:
    every line contains one data
    one line looks like this: <title>,<length>,<Categorization>,<watcher-count>

The type of the returned object should be
 java.util.Map<Catgorization, Integer>. If the file does not exists or empty,
 than it should return an empty map. Categories that are not present in the file should not be present
 in the result Map either.

*/
public class MovieStatistics {
    public static Map<Categorization, Integer> readStatistics(File inp) {
        Map<Categorization, Integer> stats = new LinkedHashMap<>();
        try {
            Scanner sc = new Scanner(inp);

            while (sc.hasNextLine()) {
                String line = sc.nextLine();

                String delims = "[,]";
                String[] strarray = line.split(delims);

                Categorization cat = Categorization.valueOf(strarray[2]);
                int watch = Integer.parseInt(strarray[3]);

                if (!(stats.isEmpty()) && stats.containsKey(cat)) {
                    stats.replace(cat, stats.get(cat) + watch);

                } else {
                    stats.put(cat, watch);
                }


            }
        } catch (FileNotFoundException fnf) {
            System.out.println("File not found");

        }

        return stats;
    }
}
